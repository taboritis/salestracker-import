## Przyjęte założenia
- Rozwiązanie zadania oparte zostało na frameworku Laravel. Wprawdzie polecenie dotyczyło Yii, ale w ofercie zamieszczonej na Pracuj.pl Laravel był wskazany jako jeden z możliwych frameworków. Niestety nie znam jeszcze Yii.
- Przy pierwszym logowaniu użytkownik powinien zmienić hasło. 
- Nie ma możliwości samodzielnej rejestracji użytkowników.
- Nie ma możliwości stworzenia użytkowników o takim samym adresie email. 
- Założyłem, że dostawca usługi email może narzucać ograniczenia, co do ilości wiadomości email na sekundę / minutę. W celach testowych korzystam z [Mailtrap]([http://mailtrap.io), które posiada dość spore ograniczenia. W rozwiązaniu ograniczyłem ilość wysyłanych maili do 6 na minutę (1 na 10 sekund). Można to zmienić w pliku config/mail.php

## Instalacja i obsługa

### Zainstaluj wymagane pakiety
```
composer install
cp .env.example .env
```

### Przygotuj bazę danych
``` 
mysql -uroot
create database YOUR_DB_NAME;
```

W pliku .env podaj nazwę utworzonej przez Ciebie bazy danych
```
DB_DATABASE=YOUR_DB_NAME
```

### Utworzenie struktury bazy danych oraz pierwszego użytkownika
```
php artisan migrate:fresh --seed
```

### Zainstaluj i skompiluj frontend 
```
npm install
npm run production
```

### Uruchomienie workera obsługującego wysyłkę powiadomień email
```
php artisan queue:work --tries=1
```
### Otwórz aplikację
Jeżeli używasz Laravel Valet
```
valet open
```
w innym wypadku
```
php artisan serve
```
I otwórz przeglądarkę wywołując adres zwrócony przez poprzednią komendę


### Logowanie do aplikacji
Import będzie możliwy tylko, gdy użytkownik jest zalogowany.
Utworzono pierwszego użytkownika
```
login: admin@salestracker.taboritis.pl
pasword: password
```

### Logowanie do serwisu Mailtrap
Na potrzeby zadania utworzyłem w serwisie mailtrap konto:
```
login: admin@salestracker.taboritis.pl
password: my-secret-password
```

Na stronie [https://mailtrap.io/inboxes/766367/messages](https://mailtrap.io/inboxes/766367/messages) znajduje się instrukcja, w jaki dokonać integracji tego konta z aplikacją opartą na Laravelu.

W przypadku korzystania z innych serwisów lub np. przekazywania wiadomości email tylko do loga aplikacji należy odpowiednio dostosować konfigurację.

### Resetowanie bazy danych
W aplikacji zaszyłem dodatkową funkcjonalność (zamieszczona pod nazwą użytkownika, w prawym górnym rogu) pozwalającą na reset bazy danych do początkowej zawartości (z tylko jednym użytkownikiem)

### Zmiana liczby dni, po których wymagany jest reset hasła
Aby zmienić liczbę dni, po których wymagany jest reset hasła wystarczy zmienić wartość [expiration](#) w pliku [config/password.php](#).
```
return [
    /**
     * How many days password is recognized as unexpired  
     */
    'expiration' => 14,
];
```

## Przykładowe zrzuty ekranu

### Ekran logowania
![Login](./screenshots/login.png)
### Lista użytkowników (możliwe przeszukwanie)
![after_reset](./screenshots/after_reset.png)
### Upload pliku z danymi
![file](./screenshots/file.png)
### Przykładowa wiadomość przechwycona przez Mailtrap
![mailtrap](./screenshots/mailtrap.png)
### Okno resetowania hasła
![renew](./screenshots/renew.png)
### Skrót do funkcji importu
![dropdown](./screenshots/dropdown.png)
### Widok workera
![queue](./screenshots/queue.png)
### Lista użytkowników
![users](./screenshots/users.png)
### Resetowanie bazy
![users](./screenshots/reset.png)