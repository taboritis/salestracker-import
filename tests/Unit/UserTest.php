<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = create(User::class);
    }

    /** @test */
    public function it_is_defined_when_password_must_be_changed()
    {
        $this->assertIsNumeric(config('password.expiration'));
    }

    /** @test */
    public function an_user_factory_works()
    {
        $this->assertInstanceOf(User::class, $this->user);
    }

    /** @test */
    public function user_created_now_has_expired_password()
    {
        $user = create(User::class, [ 'password_updated_at' => null, 'created_at' => now() ]);
        $this->assertTrue($user->hasExpiredPassword());
    }

    /** @test */
    public function user_created_year_ago_has_expired_password()
    {
        $user = create(User::class, [
            'created_at' => now()->subYear(),
            'password_updated_at' => null,
        ]);

        $this->assertTrue($user->hasExpiredPassword());
    }
}