<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ResetDatabaseCommandTest extends TestCase
{
    use RefreshDatabase;

    private $users;

    protected function setUp(): void
    {
        parent::setUp();
        $this->users = create(User::class, [], 3);
    }

    /** @test */
    public function reset_db_command_leave_only_one_user()
    {
        $this->assertEquals(3, User::count());

        Artisan::call('refresh:database');

        $this->assertEquals(1, User::count());
    }
}