<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Class ChangePasswordTest
 * @package Tests\Feature
 */
class ChangePasswordTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @var mixed
     */
    private $user;

    /**
     * Create an user to tests
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->user = create(User::class);
    }

    /** @test */
    public function a_guest_cannot_renew_password()
    {
        $this->post('/password/renew')->assertRedirect('/login');
    }

    /** @test */
    public function auth_user_can_renew_his_password()
    {
        $this->signIn($this->user);
        $isStillOldPassword = Hash::check('password', $this->user->password);
        $this->assertTrue($isStillOldPassword);

        $inputs = [
            'old_password' => 'password',
            'password' => $password = Str::random(12),
            'password_confirmation' => $password,
        ];

        $this->post('/password/renew', $inputs)->assertRedirect('/');

        $isStillOldPassword = Hash::check('password', $this->user->fresh()->password);

        $isNewPassword = Hash::check($password, $this->user->fresh()->password);

        $this->assertFalse($isStillOldPassword);

        $this->assertTrue($isNewPassword);
    }

    /** @test */
    public function password_is_not_changed_if_old_password_is_empty()
    {
        $this->signIn($this->user);

        $inputs = [
            'old_password' => '',
            'password' => $password = Str::random(12),
            'password_confirmation' => $password,
        ];
        $this->post('/password/renew', $inputs)
            ->assertSessionHasErrors('old_password');
    }

    /** @test */
    public function a_new_password_must_be_confirmed()
    {
        $this->signIn($this->user);

        $inputs = [
            'old_password' => 'password',
            'password' => Str::random(12),
            'password_confirmation' => Str::random(10),
        ];

        $this->post('/password/renew', $inputs)
            ->assertSessionHasErrors('password');
    }
}
