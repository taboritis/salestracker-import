<?php

namespace Tests\Feature\UsersCRUD;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReadUsersTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_guest_cannot_see_users_index()
    {
        $this->get('/users')->assertRedirect('/login');
    }

    /** @test */
    public function an_authenticated_user_can_see_users_index()
    {
        $this->signIn(null, [ 'password_updated_at' => now(), 'created_at' => now() ]);

        $this->get('/users')
            ->assertSee('List of users')
            ->assertOk();
    }
}