<?php

namespace Tests\Feature\UsersCRUD;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UsersImportTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_guest_cannot_see_import_page()
    {
        $this->get('/import/users')->assertRedirect('/login');
    }

    /** @test */
    public function an_auth_user_can_see_import_page()
    {
        $this->signIn(null, [ 'password_updated_at' => now(), 'created_at' => now() ]);
        $this->get('/import/users')->assertSee('Load a xls file to import users');
    }
}