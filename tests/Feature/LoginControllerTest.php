<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    private $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = create(User::class);
    }

    /** @test */
    public function user_with_incorrect_login_cannot_log_in()
    {
        $inputs = [
            'email' => $this->faker->email,
            'password' => 'password' # declared un UserFactory
        ];

        $this->post('/login', $inputs);

        $this->assertNull(Auth::user());
    }

    /** @test */
    public function an_user_with_correct_password_is_logged_in()
    {
        $inputs = [
            'email' => $this->user->email,
            'password' => 'password' # declared un UserFactory
        ];

        $this->post('/login', $inputs);

        $this->assertEquals(Auth::id(), $this->user->id);
    }

    /** @test */
    public function an_user_with_incorrect_password_is_not_logged()
    {
        $inputs = [
            'email' => $this->user->email,
            'password' => 'not-valid-password',
        ];

        $this->post('/login', $inputs);
        $this->assertNull(Auth::user());
    }

    /** @test */
    public function an_user_without_valid_password_is_redirected_to_password_renew_page()
    {
        $this->signIn(
            null,
            [ 'password_updated_at' => now()->subMonths(3) ]
        );

        $this->get('/home')->assertRedirect('/password/renew');
    }

    /** @test */
    public function a_user_with_valid_password_can_see_dashboard()
    {
        $user = create(User::class, [
            'password_updated_at' => now(),
        ]);

        $inputs = [
            'email' => $user->email,
            'password' => 'password',
        ];

        $this->post('/login', $inputs)->assertRedirect('/home');
    }
}
