<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExpiredPasswordControllerTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = create(User::class, [
            'password_updated_at' => now()->subDays(33),
        ]);
    }

    /** @test */
    public function a_guest_is_redirected_to_login_page()
    {
        $this->get('/')->assertRedirect('/login');
    }

    /** @test */
    public function user_with_expired_password_is_redirected_to_change_password_page()
    {
        $this->withoutExceptionHandling();

        $this->signIn($this->user);

        $this->get('/')->assertRedirect('/password/renew');
    }

    /** @test */
    public function a_guest_cannot_see_renew_page()
    {
        $this->get('/password/renew')->assertRedirect('/login');
    }

    /** @test */
    public function a_user_with_expired_password_can_see_renew_form()
    {
        $this->signIn($this->user);
        $this->get('/password/renew')
            ->assertSee('you have to reset');
    }
}
