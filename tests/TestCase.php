<?php

namespace Tests;

use App\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function signIn(User $user = null, $attributes = [])
    {
        $user = $user ?? create(User::class, $attributes);
        $this->actingAs($user);
        return $user;
    }
}
