<?php

use Illuminate\Support\Facades\Artisan;

Auth::routes();

Route::middleware([ 'auth' ])->group(function () {
    Route::get('/password/renew', 'ExpiredPasswordsController@form');
    Route::post('/password/renew', 'ExpiredPasswordsController@change');
});

Route::middleware([ 'auth', 'unexpired' ])->group(function () {
    Route::get('/', 'UsersController@index')->name('home');
    Route::get('/home', 'UsersController@index');
    Route::get('/users', 'UsersController@index');
    Route::get('/import/users', 'UsersController@import');
    Route::get('/database-reset', function () {
        Artisan::call('refresh:database');
        return redirect('/');
    });
    Route::post('/import/users', 'UsersController@store');
});

