<?php

return [
    /**
     * How many days password is recognized as unexpired
     */
    'expiration' => 14,
];