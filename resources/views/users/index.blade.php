@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="card card-body card-accent-primary">
      <div class="d-flex">
        <h3 class="mr-auto">List of users</h3>
        <div class="dropdown open">
          <button class="btn btn-sm btn-primary dropdown-toggle"
                  type="button" data-toggle="dropdown">
            Actions
          </button>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="/import/users">Import users</a>
            <a class="dropdown-item disabled" href="#">Create a new user</a>
          </div>
        </div>
      </div>
      <hr>
      <users-index></users-index>
    </div>
@stop