@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header">
            Load a xls file to import users.
          </div>
          <div class="card-body">
            @if (session('status'))
              <div class="alert alert-success" role="alert">
                {{ session('status') }}
              </div>
            @endif
            <form action="/import/users" method="POST" enctype="multipart/form-data">
              @csrf
              <div class="form-group">
                <label>Send a file:</label>
                <input type="file" class="form-control-file" name="usersTable">
                <small class="form-text text-muted">Your file MUST contains exactly 4 fields (columns).</small>
              </div>
              <div class="form-group">
                <button type="submit" class="form-control btn btn-outline-primary">Send a file</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
