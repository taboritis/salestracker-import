@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-6">
        <div class="card">
          <div class="card-header bg-danger text-white">
            Due to security reasons you have to reset your password.
          </div>
          <form action="/password/renew" method="POST">
            @csrf
            <div class="card-body">
              <div class="form-group">
                <label for="old_password">Old password:</label>
                <input type="password" name="old_password" id="old_password" class="form-control" required>
              </div>
              <div class="form-group">
                <label for="password">New Password:</label>
                <input type="password" name="password" id="password" class="form-control" required>
              </div>
              <div class="form-group">
                <label for="password_confirmation">Confirm a new password:</label>
                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control"
                       required>
              </div>
              <div class="form-group">
                <button type="submit" class="form-control btn btn-outline-danger">Change password</button>
              </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  </div>
@stop