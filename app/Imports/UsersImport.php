<?php

namespace App\Imports;

use App\User;
use Exception;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Jobs\SendNotificationToNewUser;

/**
 * Class UsersImport
 * @package App\Imports
 */
class UsersImport implements ToModel
{
    /**
     * @var array
     */
    private $row;

    /**
     * @param array $row
     *
     * @return User|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Model[]|null
     * @throws Exception
     */
    public function model(array $row)
    {
        $this->row = $row;

        $birthdayDate = $this->validatedBirthdayDate();
        $email = $this->validatedEmail();
        $password = Str::random(12);

        SendNotificationToNewUser::dispatch($email, $password)->delay(1);

        return new User([
            'first_name' => $row[0],
            'name' => $row[1],
            'email' => $email,
            'birthday' => $birthdayDate,
            'password' => Hash::make($password, [ 'rounds' => 4 ]),
            'api_token' => Str::random(10),
        ]);
    }

    /**
     * @param array $row
     *
     * @return false|string
     */
    private function sanitizeExcelDate()
    {
        $excelDate = $this->row[3];
        $unixDate = ($excelDate - 25569) * 86400;
        $excelDate = 25569 + ($unixDate / 86400);
        $unixDate = ($excelDate - 25569) * 86400;
        $formatted = gmdate("Y-m-d", $unixDate);
        return $formatted;
    }

    private function validatedEmail()
    {
        $email = $this->row[2];

        if (User::where('email', $email)->count()) {
            throw new Exception("Email {$email} has been already created. You cannot duplicate email.");
        }

        return $email;
    }

    private function validatedBirthdayDate()
    {
        $date = $this->row[3];
        if (is_numeric($date)) $date = $this->sanitizeExcelDate();

        return $date;
    }
}
