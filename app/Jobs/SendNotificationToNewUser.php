<?php

namespace App\Jobs;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Notification;
use App\Notifications\YourAccountHasBeenCreated;

/**
 * Class SendNotificationToNewUser
 * @package App\Jobs
 */
class SendNotificationToNewUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    private $user;

    /**
     * SendNotificationToNewUser constructor.
     *
     * @param string $email
     * @param string $password
     */
    public function __construct(string $email, string $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * Execute the job.
     * @return void
     */
    public function handle()
    {
        $this->user = User::where('email', $this->email)->firstOrFail();

        Notification::send($this->user, new YourAccountHasBeenCreated($this->email, $this->password));
    }
}
