<?php

namespace App;

use Carbon\Carbon;
use App\Filters\UsersFilters;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 * @package App
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * @var array
     */
    protected $appends = [ 'hasExpiredPassword' ];

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $guarded = [ 'id' ];

    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token',
    ];

    /**
     * The attributes that should be cast to native types.
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @param Builder $query
     * @param UsersFilters $filters
     *
     * @return Builder
     */
    public function scopeFilter(Builder $query, UsersFilters $filters)
    {
        return $filters->apply($query);
    }

    /**
     * @return bool
     */
    public function hasExpiredPassword()
    {
        $passwordLastUpdate = $this->password_updated_at;

        if (!$passwordLastUpdate) return true;

        $diffInDays = now()->diffInDays(Carbon::parse($passwordLastUpdate));

        return ($diffInDays > config('password.expiration')) ? true : false;
    }

    /**
     * @return bool
     */
    public function getHasExpiredPasswordAttribute()
    {
        return $this->hasExpiredPassword();
    }
}
