<?php

/**
 * Shortcut to factory and method create
 *
 * @param string $class
 * @param array $attributes
 * @param null $times
 *
 * @return mixed
 */
function create(string $class, $attributes = [], $times = null)
{
    return factory($class, $times)->create($attributes);
}

/**
 * Shortcut to factory and method make
 *
 * @param string $class
 * @param array $attributes
 * @param null $times
 *
 * @return mixed
 */
function make(string $class, $attributes = [], $times = null)
{
    return factory($class, $times)->make($attributes);
}

function existedOrNew(string $class, array $attributes = [])
{
    $records = $class::where($attributes);

    return ($records->count() > 0) ? $records->inRandomOrder()->first() : create($class, $attributes);
}

