<?php

namespace App\Http\Requests;

use App\Rules\OldPasswordMatch;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ChangePasswordRequest
 * @package App\Http\Requests
 */
class ChangePasswordRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => [ 'required', new OldPasswordMatch() ],
            'password' => 'required|confirmed|min:8',
        ];
    }
}
