<?php

namespace App\Http\Middleware;

use Closure;
use Exception;

/**
 * Class PasswordNotExpired
 * @package App\Http\Middleware
 */
class PasswordNotExpired
{
    /**
     * @param $request
     * @param Closure $next
     *
     * @return mixed
     * @throws Exception
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        return ($user->hasExpiredPassword())
            ? redirect('/password/renew')
            : $next($request);
    }
}
