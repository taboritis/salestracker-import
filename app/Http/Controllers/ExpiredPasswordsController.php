<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use App\Http\Requests\ChangePasswordRequest;

/**
 * Class ExpiredPasswordsController
 * @package App\Http\Controllers
 */
class ExpiredPasswordsController extends Controller
{
    /**
     * ExpiredPasswordsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function form()
    {
        return view('auth.passwords.renew');
    }

    /**
     * @param ChangePasswordRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function change(ChangePasswordRequest $request)
    {
        $request->user()->fill([
            'password' => Hash::make($request->password, [ 'rounds' => 4 ]),
            'password_updated_at' => now(),
        ])->save();

        return redirect('/');
    }
}
