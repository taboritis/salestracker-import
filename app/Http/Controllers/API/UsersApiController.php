<?php

namespace App\Http\Controllers\API;

use App\User;
use App\Filters\UsersFilters;
use App\Http\Controllers\Controller;

class UsersApiController extends Controller
{
    public function index(UsersFilters $filters)
    {
        $limit = request('limit') ?? 5;

        return User::filter($filters)->paginate($limit);
    }
}