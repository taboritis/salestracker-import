<?php

namespace App\Filters;

/**
 * Class UsersFilters
 * @package App\Filters
 */
class UsersFilters extends Filters
{
    /**
     * Recognized filters
     * @var array
     */
    protected $filters = [ 'first_name', 'name', 'email' ];

    /**
     * Filter by first name
     *
     * @param $string
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function first_name($string)
    {
        return $this->like('first_name', $string);
    }

    /**
     * Filter by user name
     *
     * @param string $string
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function name(string $string)
    {
        return $this->like('name', $string);
    }

    /**
     * Filter by user email
     *
     * @param string $email
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function email(string $email)
    {
        return $this->like('email', $email);
    }
}