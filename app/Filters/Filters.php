<?php

namespace App\Filters;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class Filters
 * @package App\Filters
 */
abstract class Filters
{
    /**
     * @var Request
     */
    private $request;

    /**
     * Elo
     * @var Builder
     */
    protected $builder;

    /**
     * @var array
     */
    protected $filters = [];

    /**
     * Filters constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply the filters.
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function apply($builder)
    {
        $this->builder = $builder;

        foreach ($this->getFilters() as $filter => $value) {
            if (method_exists($this, $filter)) {
                if ($value != null) {
                    $this->$filter($value);
                }
            }
        }

        return $this->builder;
    }

    /**
     * Fetch filters from the request.
     * @return array
     */
    public function getFilters()
    {
        return $this->request->only($this->filters);
    }

    /**
     * @param string $field
     * @param string $string
     *
     * @return Builder
     */
    public function like(string $field, string $string)
    {
        return $this->builder->where($field, 'like', "%{$string}%");
    }
}