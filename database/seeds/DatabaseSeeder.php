<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     * @return void
     */
    public function run()
    {
        create(User::class, [
            'first_name' => 'Salestracker',
            'name' => 'Admin',
            'email' => 'admin@salestracker.taboritis.pl',
            'password_updated_at' => null,
            'password' => Hash::make(env('ADMIN_PASSWORD', 'password'), [ 'rounds' => 4 ]),
            'created_at' => now(),
        ]);
    }
}
